package me.jamiemansfield.rgsconv;

import static java.util.Arrays.asList;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import me.jamiemansfield.lorenz.MappingSet;
import me.jamiemansfield.lorenz.io.reader.MappingsReader;
import me.jamiemansfield.lorenz.io.writer.MappingsWriter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

public final class RgsConvMain {

    public static void main(final String[] args) {
        final OptionParser parser = new OptionParser();

        final OptionSpec<Void> helpSpec = parser.acceptsAll(asList("?", "help"), "Show the help")
                .forHelp();

        final OptionSpec<Path> mappingInPathSpec = parser.accepts("mappingsIn", "The location of the mappings (in RGS format)")
                .withRequiredArg()
                .withValuesConvertedBy(PathValueConverter.INSTANCE);
        final OptionSpec<MappingFormat> mappingsOutFormatSpec = parser.accepts("mappingsOutFormat", "The format of the mappings")
                .withRequiredArg()
                .ofType(MappingFormat.class)
                .defaultsTo(MappingFormat.SRG);
        final OptionSpec<Path> mappingsOutPathSpec = parser.accepts("mappingsOut", "Where to save mappings")
                .withRequiredArg()
                .withValuesConvertedBy(PathValueConverter.INSTANCE);

        final OptionSet options;
        try {
            options = parser.parse(args);
        } catch (final OptionException ex) {
            System.err.println("Failed to parse OptionSet! Exiting...");
            ex.printStackTrace(System.err);
            System.exit(-1);
            return;
        }

        if (options == null || options.has(helpSpec)) {
            try {
                parser.printHelpOn(System.err);
            } catch (final IOException ex) {
                System.err.println("Failed to print help information!");
                ex.printStackTrace(System.err);
            }
            System.exit(-1);
            return;
        }

        final Path mappingInPath = options.valueOf(mappingInPathSpec);
        final MappingFormat mappingsOutFormat = options.valueOf(mappingsOutFormatSpec);
        final Path mappingsOutPath = options.valueOf(mappingsOutPathSpec);

        if (!Files.exists(mappingInPath)) {
            throw new RuntimeException("Jar in, mappings, or both do not exist!");
        }

        final MappingSet mappings = MappingSet.create();

        try (final MappingsReader reader = new RgsReader(new BufferedReader(new InputStreamReader(Files.newInputStream(mappingInPath))))) {
            reader.parse(mappings);
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }

        try (final MappingsWriter writer = mappingsOutFormat.create(new PrintWriter(Files.newOutputStream(mappingsOutPath)))) {
            writer.write(mappings);
        }
        catch (final IOException ex) {
            ex.printStackTrace();
        }
    }

    private RgsConvMain() {
    }

}
