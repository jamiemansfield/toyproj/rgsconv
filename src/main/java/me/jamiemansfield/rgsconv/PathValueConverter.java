package me.jamiemansfield.rgsconv;

import joptsimple.ValueConverter;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * An implementation of {@link ValueConverter} for handling {@link Path}s.
 *
 * @author Jamie Mansfield
 */
public final class PathValueConverter implements ValueConverter<Path> {

    public static final PathValueConverter INSTANCE = new PathValueConverter();

    @Override
    public Path convert(final String value) {
        return Paths.get(value);
    }

    @Override
    public Class<Path> valueType() {
        return Path.class;
    }

    @Override
    public String valuePattern() {
        return null;
    }

}
