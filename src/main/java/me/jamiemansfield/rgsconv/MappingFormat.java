package me.jamiemansfield.rgsconv;

import me.jamiemansfield.lorenz.io.reader.CSrgReader;
import me.jamiemansfield.lorenz.io.reader.MappingsReader;
import me.jamiemansfield.lorenz.io.reader.SrgReader;
import me.jamiemansfield.lorenz.io.reader.TSrgReader;
import me.jamiemansfield.lorenz.io.writer.CSrgWriter;
import me.jamiemansfield.lorenz.io.writer.MappingsWriter;
import me.jamiemansfield.lorenz.io.writer.SrgWriter;
import me.jamiemansfield.lorenz.io.writer.TSrgWriter;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.function.Function;

/**
 * The many mapping formats that are supported by rgsconv's CLI, all provided
 * by Lorenz.
 *
 * @author Jamie Mansfield
 */
public enum MappingFormat {

    /**
     * The standard SRG mapping format.
     */
    SRG(SrgReader::new, SrgWriter::new),

    /**
     * The compact SRG mapping format, used by Spigot.
     */
    CSRG(CSrgReader::new, CSrgWriter::new),

    /**
     * The tabbed SRG mapping format, used by MCPConfig.
     */
    TSRG(TSrgReader::new, TSrgWriter::new),

    ;

    private final Function<BufferedReader, MappingsReader> readerConstructor;
    private final Function<PrintWriter, MappingsWriter> writerConstructor;

    MappingFormat(final Function<BufferedReader, MappingsReader> readerConstructor,
            final Function<PrintWriter, MappingsWriter> writerConstructor) {
        this.readerConstructor = readerConstructor;
        this.writerConstructor = writerConstructor;
    }

    /**
     * Creates a mapping reader for the given format.
     *
     * @param reader The reader to use for construction
     * @return The mapping reader
     */
    public MappingsReader create(final BufferedReader reader) {
        return this.readerConstructor.apply(reader);
    }

    public MappingsWriter create(final PrintWriter writer) {
        return this.writerConstructor.apply(writer);
    }

}
