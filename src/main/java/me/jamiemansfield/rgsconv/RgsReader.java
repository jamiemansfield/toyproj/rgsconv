package me.jamiemansfield.rgsconv;

import me.jamiemansfield.lorenz.MappingSet;
import me.jamiemansfield.lorenz.io.reader.MappingsProcessor;
import me.jamiemansfield.lorenz.io.reader.MappingsReader;
import me.jamiemansfield.lorenz.io.reader.SrgProcessor;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.stream.Stream;

public class RgsReader extends MappingsReader {

    public RgsReader(final BufferedReader reader) {
        super(reader, Processor::new);
    }

    public static class Processor extends MappingsProcessor {

        private static final String CLASS_MAPPING_KEY = ".class_map";
        private static final String FIELD_MAPPING_KEY = ".field_map";
        private static final String METHOD_MAPPING_KEY = ".method_map";

        private static final int CLASS_MAPPING_ELEMENT_COUNT = 3;
        private static final int FIELD_MAPPING_ELEMENT_COUNT = 3;
        private static final int METHOD_MAPPING_ELEMENT_COUNT = 4;

        Processor(final MappingSet mappings) {
            super(mappings);
        }

        @Override
        public boolean processLine(final String rawLine) throws IOException {
            Stream.of(rawLine)
                    // Handle comments, by removing them.
                    // This implementation will allow comments to be placed anywhere
                    .map(SrgProcessor::removeComments)
                    // Trim the line
                    .map(String::trim)
                    // Filter out empty lines
                    .filter(line -> !line.isEmpty())
                    // Process line
                    .forEach(line -> {
                        if (line.length() < 4) {
                            System.out.println("Faulty RGS mapping encountered: `" + line + "` - ignoring");
                            return;
                        }
                        // Split up the line, for further processing
                        final String[] split = SPACE.split(line);
                        final int len = split.length;

                        // Establish the type of mapping
                        final String key = split[0];
                        if (key.equals(CLASS_MAPPING_KEY) && len == CLASS_MAPPING_ELEMENT_COUNT) {
                            final String obfuscatedName = split[1];
                            final String deobfuscatedName = split[2];

                            // Get mapping, and set de-obfuscated name
                            this.mappings.getOrCreateClassMapping(obfuscatedName)
                                    .setDeobfuscatedName(deobfuscatedName);
                        } else if (key.equals(FIELD_MAPPING_KEY) && len == FIELD_MAPPING_ELEMENT_COUNT) {
                            final String fullObfuscatedName = split[1];
                            final String deobfuscatedName = split[2];
                            final int lastIndex = fullObfuscatedName.lastIndexOf('/');
                            final String owningClass = fullObfuscatedName.substring(0, lastIndex);
                            final String obfuscatedName = fullObfuscatedName.substring(lastIndex + 1);

                            // Get mapping, and set de-obfuscated name
                            this.mappings.getOrCreateClassMapping(owningClass)
                                    .getOrCreateFieldMapping(obfuscatedName)
                                    .setDeobfuscatedName(deobfuscatedName);
                        } else if (key.equals(METHOD_MAPPING_KEY) && len == METHOD_MAPPING_ELEMENT_COUNT) {
                            final String fullObfuscatedName = split[1];
                            final String obfuscatedSignature = split[2];
                            final String fullDeobfuscatedName = split[3];
                            final int lastIndex = fullObfuscatedName.lastIndexOf('/');
                            final String owningClass = fullObfuscatedName.substring(0, lastIndex);
                            final String obfuscatedName = fullObfuscatedName.substring(lastIndex + 1);
                            final String deobfuscatedName = fullDeobfuscatedName.substring(fullDeobfuscatedName.lastIndexOf('/') + 1);

                            // Get mapping, and set de-obfuscated name
                            this.mappings.getOrCreateClassMapping(owningClass)
                                    .getOrCreateMethodMapping(obfuscatedName, obfuscatedSignature)
                                    .setDeobfuscatedName(deobfuscatedName);
                        } else {
                            System.out.println("Found unrecognised key: `" + key + "` - ignoring");
                        }
                    });
            return true;
        }

    }

}
